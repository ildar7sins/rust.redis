mod models;

use bytes::Bytes;
use std::sync::{Arc, Mutex};
use std::collections::HashMap;
use tokio::net::{TcpListener, TcpStream};
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::models::types::DB;
use crate::models::request_parser::RequestParser;


#[tokio::main]
async fn main() {
    let db: DB = Arc::new(Mutex::new(HashMap::new()));
    let listener = TcpListener::bind("127.0.0.1:6379").await.unwrap();

    loop {
        // The second item contains the IP and port of the new connection.
        let (socket, _) = listener.accept().await.unwrap();
        println!("{:?}", db.clone());
        let db = db.clone();

        tokio::spawn(async move {
            handle_request(socket, db).await;
        });
    }
}

async fn handle_request(mut stream: TcpStream, db: DB) {
    {
        let mut buffer = [0; 1024];
        stream.read(&mut buffer).await.unwrap();

        let request: String = String::from_utf8_lossy(&buffer[..]).to_string();

        let new_request = RequestParser::create(request);

        let res = match new_request {
            Ok(request) => request.parse(db),
            Err(e) => Bytes::from(e.to_string())
        };

        stream.write_all(&*res).await.unwrap();
        stream.read(&mut buffer).await.unwrap();
}
