use crate::DB;
use bytes::Bytes;

pub struct DbOperation {}

impl DbOperation {
	pub fn set(db: DB, key: String, value: Bytes) -> Bytes {
		let mut db = db.lock().unwrap();
		db.insert(key, value);

		Bytes::from("+OK\r\n")
	}

	pub fn get(db: DB, key: String) -> Bytes {
		let db = db.lock().unwrap();
		if let Some(value) = db.get(&*key) {
			Bytes::from(format!("+{}\r\n", std::str::from_utf8(&value.clone()).unwrap()))

		} else {
			Bytes::from("+None\r\n")
		}
	}

	pub fn update(db: DB, key: String, value: Bytes) -> Bytes {
		let mut db = db.lock().unwrap();
		db.insert(key, value);

		Bytes::from("+OK\r\n")
	}

	pub fn delete(db: DB, key: String) -> Bytes {
		let mut db = db.lock().unwrap();
		db.remove(&*key);

		Bytes::from("+OK\r\n")
	}
}

