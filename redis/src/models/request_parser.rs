use crate::DB;
use bytes::{Bytes};
use crate::models::types::RedisRequestType;
use crate::models::db_operation::DbOperation;

#[derive(Debug)]
pub struct RequestParser {
	pub request: String,
	pub comand: RedisRequestType,
	pub data: Vec<String>
}

impl RequestParser {
	pub fn create(data: String) -> Result<RequestParser<>, &'static str> {
		let request = data.clone();
		let data_vec: Vec<String> = data.lines().map(str::to_string).collect();

		if data_vec.len() > 4 {
			let comand = match data_vec[2].as_str() {
				"SET" => Ok(RedisRequestType::SET),
				"GET" => Ok(RedisRequestType::GET),
				"UPDATE" => Ok(RedisRequestType::UPDATE),
				"DELETE" => Ok(RedisRequestType::DELETE),
				_ => Err("Unknown ype")
			};

			match comand {
				Ok(comand_val) => Ok(RequestParser{request, comand: comand_val, data: data_vec}),
				Err(e) => Err(e)
			}
		} else {
			Err("Bad request")
		}
	}

	pub fn parse(&self, db: DB) -> Bytes {
		use crate::models::types::RedisRequestType::*;

		let key: String = self.get_key();

		match self.comand {
			SET => DbOperation::set(db, key, self.get_value()),
			GET => DbOperation::get(db, key),
			UPDATE => DbOperation::update(db, key, self.get_value()),
			DELETE => DbOperation::delete(db, key),
		}
	}

	pub fn get_key(&self) -> String {
		self.data[4].clone()
	}

	pub fn get_value(&self) -> Bytes {
		Bytes::from(self.data[6].clone())
	}
}
