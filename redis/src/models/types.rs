
use bytes::Bytes;
use std::sync::{Arc, Mutex};
use std::collections::HashMap;

pub type DB = Arc<Mutex<HashMap<String, Bytes>>>;

#[derive(Debug)]
pub enum RedisRequestType {
	SET,
	GET,
	UPDATE,
	DELETE
}
