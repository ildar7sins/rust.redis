use async_std::io;
use async_std::net::{TcpStream};
use async_std::prelude::*;

#[async_std::main]
async fn main() -> io::Result<()>
{
	let _set = set().await;
	let _get = get().await;

	Ok(())
}


async fn set() -> Result<String, Error> {
	let mut stream = TcpStream::connect("127.0.0.1:6379").await?;

	stream.write_all(b"*3\r\n$3\r\nSET\r\n$6\r\nmy_key\r\n$6\r\nmy_key\r\n").await?;

	let mut buffer = [0; 1024];

	let bytes_read = stream.read(&mut buffer).await?;
	let resp = parse_response(&buffer[..bytes_read])?;

	println!("{}", resp.to_owned());

	Ok("RES".to_string())
}

async fn get() -> Result<String, Error> {
	let mut stream = TcpStream::connect("127.0.0.1:6379").await?;

	stream.write_all(b"*2\r\n$3\r\nGET\r\n$6\r\nmy_key\r\n").await?;

	let mut buffer = [0; 1024];

	let bytes_read = stream.read(&mut buffer).await?;
	let resp = parse_response(&buffer[..bytes_read])?;

	println!("{}", resp.to_owned());

	Ok("RES".to_string())
}


fn parse_response(buffer: &[u8]) -> Result<&str, Error> {
	if buffer.is_empty() {
		return Err(Error {});
	}

	if buffer[0] == ('-' as u8) {
		return Err(Error {});
	}

	Ok(std::str::from_utf8(&buffer[1..buffer.len() - 2]).unwrap())
}


#[derive(Debug)]
struct Error {}

impl std::convert::From<io::Error> for Error {
	fn from(_e: io::Error) -> Self {
		Error {}
	}
}
